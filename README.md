## Namaste 🙏, Rohit here!

* I am a Product Designer working at [Square Off](https://www.squareoffnow.com) in the product side of things.
* Here's the link to my [portfolio](https://rohitmethwani.xyz)
* I am a resident of Mumbai, India 🇮🇳. I did my graduation in Computer Engineering from Mumbai University in the year 2021.
* My experience have been very mixed as I've worked as a Developer and Designer throughtout. Here's a little bit more about my journey till now.
  * 2017 - Worked as a Freelance Graphic Designer
  * 2018 - Worked as Freelance Frontend Developer
  * 2018-2021 - Worked as Freelance UI Designer and Frontend Developer
  * 2021 - Joined Thence Design Studio as a UI Designer
  * 2021 - Left Thence Design Studio to move at Square Off as a UI/UX Designer
  * Since 2021 - Have been working fulltime


### Personality
1. My personality type is [INFP-T](https://www.16personalities.com/infp-personality). The Mediator.
2. I am very [Expressive](https://www.proprofs.com/quiz-school/story.php?title=what-is-my-social-style)!


### Personal Principles
1. Help needy as much as possible
2. Stay humble
3. Impact others with the work I do
4. Always stay thankful for life


## 💼 Work

These are some things which I've been working on since 2019.

### Square Off
Although I am not allowed to details much but I can surely share the overview of all the things I've done at Square Off in nutshell. As a sole Product Designer, I

1. Designed an asynchronous support experience that would help users troubleshoot problems and solve them that helped reduce customer support queries by ~25% and prevent unnecessary replacements saving money for the company
2. Improved users that are onboarding have a seamless experience and also decreasing initial drop-offs by ~30% and TAT till the first game
3. Conducted the first experience-led campaign to understand user behavior. Talked with users and got to know their perspectives about the product
4. Optimized rating system to improve rating from 3.9 to 4.3 on the App store
5. Moved focus to an “Experience-first” approach from earlier followed “Feature-first” approach
6. Coordinated with developers on features & design hand-off
7. Presented design solutions to stakeholders, resulting in greater support for design decisions and direction
8. Develop and maintain a design system ensuring consistency and scalability across the product


### Thence Design Studio
As an UI Design Intern, I

1. Worked in a team of 5 with 3 fellow designers and a Product Lead where in we designed a solution for a fitness platform for a sports giant-Royal Challengers Bangalore (RCB)
2. Was responsible for designing the module for trainers which would help them to conduct online workshops seamlessly, along with managing the design documentation and handoff part.


### All other work
Please check my [resume](https://rohitmethwani.xyz/files/Resume_Rohit_Methwani(Product%20Designer).pdf) for more details. 


## 🗣️ My coworkers have said this...

> He always pushes hard and keeps on iterating the stuff. He has been very critical about all the aspects of the product and never dissapoints with his outcomes. I've seen him working with developers and considering their constraints, he makes sure we do the right things with these constraints as well. Kudos for the great work!
> 
> _[Rushikesh Dumbare](https://www.linkedin.com/in/rushikesh-dumbre-57680946/), Product Manager (current). Square Off_


> Rohit is a great professional to work with. We worked together at Square Off & some freelance projects earlier and he helped me a lot to get started and also understand more about product design in terms of UI & UX. His work ethic is immaculate and so easy to work together with. He goes out of his way when you ask for some help and guidance.
> 
> _[Hardik Somaiya](https://www.linkedin.com/in/hardiksomaiya/), Product Manager (till April 2022). Square Off_


## 🤩 Achievement

In 2019, my work was endorsed in the official Adobe's blog. It is a plugin that provides ready made most used components in mobile UI design. 

- Check out the [blog](https://blog.adobe.com/en/publish/2019/10/30/adobe-xd-plugins-jumpstart-your-designs) here.
- Check out the plugin [here](https://www.figma.com/community/plugin/830533738628941885/Ready-Components) (Figma version.)


## 🙋 Apart from design...
1. I am a huge petrolhead! In 2021, I bought my first(Hyundai i10) car with my own hard-earned money. I love motor racing as well. A huge huge Formula 1 and Ferrari Fan. I have a dream to drive a Ferrari one day!
2. I also like to read and learn more about history, geography, world politics and economics. It is the most interesting thing I've gained interest in.
3. I also play guitar and sing (although I haven't learned it professionally). Have also performed in a small local concert with a band.
4. I've recently started learning Spanish. I want to learn Italian, French and Arabic as well.
5. I come from a very spiritual household and that reflects in my behavior as well!
6. I also teach design to college students. It's not very common here but I am glad I can contribute back to the community (as I myself am a self taught designer)
